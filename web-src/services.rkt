#lang _-exp brenthouse/lib/html-page-lang

#:title "Services & Programs"


#:lhs (list
       #;`(p () ,3-alphabets)
       (make-page-nav nav-spec))


(define nav-spec
  `(["#sunday"
     ("Sunday Eucharist and Supper")]
    #;
    ["#tuesday"
     ("Tuesday Morning Prayer and Table Hours")]
    ["#wednesday"
     ("Wednesday")
     (["#afternoon-tea" ("Afternoon Tea")]
      ["#evening-prayer" ("Evening Prayer")])]
    ["#thursday"
     ("Thursday Church")]))

(struct campus-place (name map-url)
  #:transparent)

(define place:brent-house
  (campus-place "Brent House"
                "https://maps.uchicago.edu/location/brent-house/"))
(define place:hutch
  (campus-place "Hutchinson Commons"
                "https://maps.uchicago.edu/location/hutchinson-commons/"))
(define place:bond
  (campus-place "Bond Chapel"
                "https://maps.uchicago.edu/location/bond-chapel/"))

(define (fall21-event-header #:title title
                             #:time time
                             #:place place)
  (match-define (campus-place name map-url) place)
  `(header ([class "fall21-event-header"])
           (h4 () ,@title)
           (div ()
                (p () ,@time)
                (p () ,name " " ,(link-out map-url #:new-window? #t
                                           "Directions "
                                           (icon:open-in-new #:inline? #t))))))
#;
(define tea+dinner
  `(["February 16" "https://www.facebook.com/events/485163966588480/"
     "https://www.facebook.com/events/1001711207223136/"]
    ["February 23" "https://www.facebook.com/events/494132382441134/"
     "https://www.facebook.com/events/326582159422407/"]
    ["March 2" "https://www.facebook.com/events/791316455595901/"
     "https://www.facebook.com/events/652630989295042/"]
    ["March 9" "https://www.facebook.com/events/1362259744212341/"
     "https://www.facebook.com/events/272713984978795/"]
    ["March 16" "https://www.facebook.com/events/460073652433605/"
     "https://www.facebook.com/events/989937718627562/"]))

#:body

(div ([class "flow-root"])
     (div ([class "pandemic-update welcome-residents-hack"])
          (div ()
               (p "Services resume on Sunday, September 25."))))

ƒp{
 In addition to our main service on
 ƒ(a ([href "#sunday"]) "Sunday evenings")
 in the Brent House chapel, we have
 ƒ(a ([href "#thursday"]) "“Thursday Church”"),
 a simple midday Eucharist in Bond Chapel on campus.
}
ƒp{
 ƒ;On Tuesdays,
 ƒ;join Alberto in Hutch for
 ƒ;ƒ(a ([href "#tuesday"]) "Morning Prayer")
 ƒ;or just to chat.
 Wednesday is Brentsday:
 whether you’re looking for a quiet place to study
 or a relaxing break with great conversation,
 the house is open for
 ƒ(a ([href "#afternoon tea"]) "afternoon tea"),
 followed by a contemplative service of
 ƒ(a ([href "#evening-prayer"]) "Evening Prayer").
 Afterwords, join us for a home-cooked
 ƒ(a ([href "#evening-prayer"]) "dinner") and weekly speaker,
 movie, discussion, or other event.
}
ƒp{
 In addition to these weekly events, Brent House hosts
 special programs throughout the year. You can find more
 details on our ƒ,link-out["https://www.facebook.com/brent.house.chicago/events"]{
  Facebook} page and in our weekly ƒ,link-out["https://newsletter.brenthouse.org"]{
  newsletter}, and your RSVP to the Facebook events helps us plan.
}

(section
 ([id "sunday"])
 (h3 "Sunday")
 ,(fall21-event-header #:title `ƒ{Holy Eucharist and Supper}
                       #:time `ƒ{5:30ƒ|ndash|7:00 ƒ,(pm)}
                       #:place place:brent-house)
 ƒp{ We’ll be back in our basement chapel for a service of
 Holy Communion with sermon and communal reflection, Brent
 House style, followed by a vegetarian supper. Your RSVP to
 the Facebook event helps us plan. }
 ,(floater-img "/media/chapel-candles.jpg"
               "The Brent House chapel with many candles")
 #;
 (ul ()
     ,@(map (match-lambda
              [(list day u)
               `(li () ,(link-out u day))])
            `(["February 20" "https://www.facebook.com/events/1013518962888824/"]
              ["February 27" "https://www.facebook.com/events/482175293309330/"]
              ["March 6" "https://www.facebook.com/events/951246408860571/"]
              ["March 13" "https://www.facebook.com/events/792053718417239/"]))))

#;
(section
 ([id "tuesday"])
 (h3 "Tuesday")
 ,(fall21-event-header
   #:title `ƒ{Morning Prayer and Table Hours}
   #:time `ƒ{10:15 ƒ,(am) ƒ|ndash| 1:15 ƒ,(pm)}
   #:place place:hutch)
 ƒp{ Join our seminary intern, Alberto Varona, for Morning
 Prayer, a simple service of scripture and prayer at 10:15 ƒ,(am),
 or drop by to say hello any time before 1:15 ƒ,(pm)! })


(section
 ([id "wednesday"])
 (h3 "Wednesday")
 (section
  ([id "afternoon-tea"])
  ,(fall21-event-header
    #:title `ƒ{Afternoon Tea}
    #:time `ƒ{2:00ƒ|ndash|5:00 ƒ,(pm)}
    #:place place:brent-house)
  ƒp{ Stop by the House for a cup of tea and an open time for
 conversation, studying, chatting, resting, or knitting.
 (Stacy will teach you!) Drop in when you can, stay as long
 as you like. }
  #;
  (ul ()
     ,@(map (match-lambda
              [(list day u _)
               `(li () ,(link-out u day))])
            tea+dinner)))
 (section
  ([id "evening-prayer"])
  ,(fall21-event-header
    #:title `ƒ{Evening Prayer}
    #:time `ƒ{6:00ƒ|ndash|6:30 ƒ,(pm)}
    #:place place:brent-house)
  ƒp{Mark the transition from day to evening with this
 ancient form of prayer from the monastic tradition. In the
 Brent House chapel.})
 (section
  ([id "brentsday"])
  ,(fall21-event-header
    #:title `ƒ{Brentsday Dinner & Events}
    #:time `ƒ{6:30ƒ|ndash|8:00 ƒ,(pm)}
    #:place place:brent-house)
  ƒp{Afterwords, join us for a free home-cooked vegetarian
 supper as we welcome a speaker, watch a movie, or share a
 lively discussion—see the weekƒ|rsquo|s Facebook event or the
 newsletter for details! Your RSVP helps us plan.}
  #;
  (ul ()
     ,@(map (match-lambda
              [(list day _ u)
               `(li () ,(link-out u day))])
            tea+dinner))))

(section
 ([id "thursday"])
 (h3 "Thursday")
 ,(fall21-event-header
   #:title `ƒ{Thursday Church}
   #:time `ƒ{12:30ƒ|ndash|1:00 ƒ,(pm)}
   #:place place:bond)
 ƒp{ A simple celebration of the Eucharist in Bond Chapel.
 Join us for lunch together on campus afterwards. }
 #;
 (ul ()
     ,@(map (match-lambda
              [(list day u)
               `(li () ,(link-out u day))])
            `(["February 17" "https://www.facebook.com/events/1186877638808670/"]
              ["February 24" "https://www.facebook.com/events/410820094178969/"]
              ["March 3" "https://www.facebook.com/events/4631073270337401/"]
              ["March 10" "https://www.facebook.com/events/662642188197243/"]))))

