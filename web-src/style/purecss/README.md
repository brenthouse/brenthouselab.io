Pure
====

Minification has almost no benefit over compression/`Content-Transfer-Encoding`,
so we don't use it:
```
              ┏━━━━━━┳━━━━━━━━━━━━━━━━━━┓
              ┃ pure ┃ grids-responsive ┃
┏━━━━━━━━━━━━━╉──────╂──────────────────┨
┃     .css.br ┃ 8.0K ┃ 4.0K             ┃
┣━━━━━━━━━━━━━╉──────╂──────────────────┨
┃ -min.css.br ┃ 4.0K ┃ 4.0K             ┃
┗━━━━━━━━━━━━━┻━━━━━━┻━━━━━━━━━━━━━━━━━━┛
```
