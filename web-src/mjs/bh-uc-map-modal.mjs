/**
 *
 * bh-uc-map-modal.mjs
 *
 * @license magnet:?xt=urn:btih:8e4f440f4c65981c5bf93c76d35135ba5064d8b7&dn=apache-2.0.txt Apache-2.0
 *
 */

import MicroModal from '/mjs/MicroModal.mjs'; 

MicroModal.init({
    openTrigger: 'data-bh-uc-map-modal-open',
    onShow: (modal,trigger,evt) => {
        const name = trigger.dataset.bhUcMapModalName;
        const url = trigger.dataset.bhUcMapModalHref;
        const frame = modal.querySelector('iframe');
        const link = modal.querySelector('.bh-uc-map-modal-link');
        const heading = modal.querySelector('h2');
        link.href = url;
        heading.innerText = 'Directions to ' + name;
        frame.title = 'Campus map showing ' + name + '.';
        frame.src = url;
    },
    onClose: (modal,trigger,evt) => {
        const frame = modal.querySelector('iframe');
        const link = modal.querySelector('.bh-uc-map-modal-link');
        const heading = modal.querySelector('h2');
        const deactivated = 'The map widget was deactivated.';
        link.removeAttribute('href');
        heading.innerText = deactivated;
        frame.title = deactivated;
        frame.src = 'about:blank';
    }
});


// @license-end
