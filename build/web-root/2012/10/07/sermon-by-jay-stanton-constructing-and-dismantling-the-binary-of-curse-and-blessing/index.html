<!DOCTYPE html>
<html lang="en" class="has-lhs"
><head
><title
>Sermon by Jay Stanton: "Constructing and Dismantling the Binary of Curse and Blessing" | Brent House</title><meta charset="utf-8"
/><meta name="viewport" content="width=device-width,initial-scale=1"
/><meta name="format-detection" content="telephone=no"
/><link rel="stylesheet" href="/style/fonts/fonts.css"
/><link rel="stylesheet" href="/style/old-brenthouse.css"
/><link rel="icon" type="image/svg+xml" href="/style/heresy-shield.svg"
/></head><body
><header class="site-header"
><h1
>Brent House</h1><p
>The Episcopal Center at the University of Chicago</p><div class="site-logo"
><a href="/"
><img src="/style/heresy-shield.svg" alt="Brent House’s logo"
/></a></div></header><nav class="site-nav"
><ul
><li
><a href="/"
>Welcome</a></li><li
><a href="/services.html"
>Services &amp; Programs</a></li><li
><a href="/blog/"
>Blog</a></li><li
><a href="/leadership.html"
>Clergy &amp; Leadership</a></li><li
><a href="/residents-and-guests.html"
>Living or Staying at Brent House</a></li></ul></nav><main class="page-container"
><header
><h2
>Sermon by Jay Stanton: "Constructing and Dismantling the Binary of Curse and Blessing"</h2></header><div class="page-body"
><h3
>Sunday, October 7, 2012</h3><p
>This sermon was preached by friend of the House, Jay Stanton, at KAM Isaiah here in Hyde Park:</p><p
><i
>R’eh</i> 5772</p><p
>Jay Stanton</p><p
>Constructing and Dismantling the Binary of Curse and Blessing</p><p
>This week’s <i
>parashah</i> opens by challenging us to visualize an amazing scene.  Commanding us to see, the portion starts with the word <i
>r’eh</i>:</p><p
>“See this day I set before you blessing and curse: blessing, if you obey the commandments of the Lord your God that I enjoin upon you this day; and curse if you do not obey the commandments of the Lord your God, but turn away from the path that I enjoin upon you this day and follow other gods, whom you have not experienced.  When the Lord your God brings you into the land that you are about to enter and possess, you shall pronounce the blessing at Mount Gerizim and the curse at Mount Ebal - both are on the other side of the Jordan, beyond the west road that is in the land of the Canaanites who dwell in the Arabah – near Gilgal, by the terebinths of Moreh.” (New JPS translation)</p><p
>This passage seems to present us with a binary between blessing and curse:  two mountains separated by a valley full of Israelites.  On one, we recite all the blessings of life, and on the other, all the curses.  Up one slope, we place everything good: prosperity, abundance, compassion; up the opposite slope, we place everything evil: hatred, violence, famine.  So we choose blessing, reject curse, and only good things happen to us for the rest of time.  Easy enough, right?</p><p
>Our experiences of doing good deeds and following <i
>mitzvot </i>have not banished evil from our midst.  Recent shooting sprees and climate scorching show that to us on a global level, and we all have personal examples of good deeds leading to bad consequences.  At face value, our world doesn’t reflect our Torah.</p><p
>We constructed the binary we see between blessing and curse. This passage tells the story of its construction.  My mentor Kate Bornstein teaches that all binaries are false, and I have yet to be confronted with a counterexample.  Binaries invite us to consider their fallacy; we usually gain something from their simplification while we lose something else.  Generalizations help us build a coherent picture of our surroundings, but that picture is a lot more complicated than a mountain of curses and a mountain of blessings. When we look at the text closely, this seeming binary gets more complex.</p><p
>God gives us both blessing and curse.  Moreover, both blessing and curse are present in the Promised Land.  If God wanted us to be completely free from curse, it would have been left on the other side of the Jordan, in somebody else’s land, apart, at least temporarily, from the holy people.  The Deuteronomist invites us to consider the relationship between holy people and holy space.  Yet we are required to proclaim curses within the land of Israel, the spatial realm of holiness.  Cursing is holy even when we don’t start a stream of curses with the word.  The power to curse is divine.  Made in the divine image, we share that power.</p><p
>Understanding curse as a gift from God is not easy.  The predominant culture in America views God as only good, a Force repelling evil which can help you do likewise if you profess belief.  Furthermore, when religious leaders try to explain evil in the world, they often reject the notion that evil comes from God.  Instead, they blame individuals or categories of people, often ones that include me.  So, in this post-Holocaust, post-9/11 world where genocide, terrorism, and generational poverty still afflict us on a mass scale, and joblessness, grief, and illness afflict us on a personal level, we want to divorce ourselves from any discussion of reward and punishment as God-given.  Or we take God as Rewarder-and-Punisher as a fact we must believe about the divine in order to accept its existence and reject God altogether.  This perception of unfairness on God’s part is not merely an image of an unexamined popular psyche; it permeates academia as well.  We call the inconsistency “the problem of theodicy.”</p><p
>What if theodicy is not a problem?  We live in a complex global society, where whether we remember to turn the lights off when we leave a room in Hyde Park affects people in Mongolia.  Hardly any action we take or choice that we make has consequences only impacting ourselves.  Bad things happen to people who are doing good things.  If God causes everything, then God is the cause of evil as well as good.  By presuming that God works on a merit system, we have trouble explaining suffering in the world.  I recently finished the book <i
>The Jew in the Lotus</i>, in which Rodger Kamenetz describes his experience as a witness to interfaith dialogue between Jews and Tibetan Buddhists including the Dalai Lama in Dharamsala.  Kamenetz relates a conversation about suffering between a Jew who spent time as a Buddhist and a Buddhist who was raised as a Jew.  Buddhists try to explain why suffering occurs and how we can eliminate it.  Jews acknowledge its existence and strive to live with it on a daily basis.  We don’t try to explain away our suffering; we ask what we can do to alleviate pain.</p><p
>God invites us to recognize curse and blessing as extremes.  We in the valley, or the Midwest, can’t actually live on Mount Gerizim, the mountain of blessing, either in fact or metaphorically.  Unlike the sages of Monty Python, we can’t always look on the bright side of life.  Curse and blessing surround us, and they are often not separable on distinct mountains.  A friend dies, and we realize how deeply we were impacted by her presence.  We move to a new place to pursue new opportunities, and we leave our home behind.  We use the extremes of blessing and curse to make sense of our own lives.  Most of the time, we experience both simultaneously.  Yet we navigate our world using the landmarks of Mount Ebal and Mount Gerizim.</p><p
>Often, we take this navigation system as a law.  We are overeager to call one thing a blessing and something else a curse, using our own value system to place more situations at one extreme or another than actually belong there.  We forget that both blessing and curse are gifts, and we forget that the power of each stems from its relationship to the other and their mutual juxtaposition.  They help us choose to live rightly.  They help us do <i
>tikkun olam</i>.  The presence of both blessing and curse gives us the opportunity to bring blessing to the parts of society or our lives that we view as cursed.  When we feel we are only blessed and not cursed, the presence of both curse and blessing argues against complacency.  God gave us blessing and curse in distinction to each other so we can be agents of change in our own lives and in our world.</p><p
>The rabbinic tradition suggests that we say one hundred blessings throughout the day.  Forty is a big number in Judaism, and one hundred is two-and-a-half times bigger; the task seems overwhelming.  However, we are encouraged to say blessings over the small things in life.  We say blessings over drinking a glass of water, eating, seeing a great scholar – all daily activities in Hyde Park. We pepper our day with blessings to remind us that they are present to us in the midst of the curses and that we can invoke a blessing whenever necessary.</p><p
>Blessings themselves aren’t necessarily all good, either.  A Midrash teaches us that two angels are sent to follow us on Shabbat.  One is evil, and one is good. When we keep Shabbat, the good one shares the blessing, “May your next Shabbat be exactly like this one.”  When we break Shabbat, the evil one says, “May your next Shabbat be exactly like this one.”  Tonight, I want to offer a different blessing.<br
/>In the Jewish calendar, we are in a period of reflection and self-examination.  Tomorrow night we begin the month Elul.  We take the month to start our process of <i
>t’shuvah</i>, repentance, to get ready for the High Holidays.  We are involved in a process of reconciliation with God, moving from the pain of <i
>Tisha B’av</i> – which commemorates the destruction of the Temple and other national tragedies – to the joy of <i
>Simjat Torah</i>.  So my blessing for us tonight is this: may we be different next week than we are this week, and may we be different next year than we are this year.  In the coming year, may we navigate our lives juxtaposing blessing and curse.  May we use our awareness of God’s gifts of curse and blessing to shine light in dark places.  And that way, may we bring more peace and wholeness – <i
>sh’leimut</i> – to the world.</p></div><aside
><div class="blog-nav-buttons-container"
><p class="basic-button-container"
><a href="/2012/"
><span class="rotate-quarter-turn"
><svg class="photon-icon back" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"
><!--
- This <svg> element is derrived from “back-16.svg”
- from Mozilla’s “Photon Icons” collection.
- It is subject to the terms of the Mozilla Public License, v. 2.0.
- If a copy of the MPL was not distributed with this file,
- you can obtain one at <https://mozilla.org/MPL/2.0/>.
- * Source: <https://github.com/BrentHouse/photon-icons>
- * Upstream: <https://github.com/FirefoxUX/photon-icons>
--><path fill="context-fill" d="M15 7H3.414l4.293-4.293a1 1 0 0 0-1.414-1.414l-6 6a1 1 0 0 0 0 1.414l6 6a1 1 0 0 0 1.414-1.414L3.414 9H15a1 1 0 0 0 0-2z"
></path></svg></span><br
/>Browse Archive</a></p><p class="basic-button-container"
><a href="/2015/06/20/sermon-for-the-ordination-of-nadia-stefko-to-the-diaconate/"
><svg class="photon-icon forward" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"
><!--
- This <svg> element is derrived from “forward-16.svg”
- from Mozilla’s “Photon Icons” collection.
- It is subject to the terms of the Mozilla Public License, v. 2.0.
- If a copy of the MPL was not distributed with this file,
- you can obtain one at <https://mozilla.org/MPL/2.0/>.
- * Source: <https://github.com/BrentHouse/photon-icons>
- * Upstream: <https://github.com/FirefoxUX/photon-icons>
--><path fill="context-fill" d="M15.707 7.293l-6-6a1 1 0 0 0-1.414 1.414L12.586 7H1a1 1 0 0 0 0 2h11.586l-4.293 4.293a1 1 0 1 0 1.414 1.414l6-6a1 1 0 0 0 0-1.414z"
></path></svg><br
/>June 20, 2015<br
/><span class="nav-button-post-title"
>Sermon for the Ordination of Nadia Stefko to the Diaconate</span></a></p><p class="basic-button-container"
><a href="/2011/07/06/sermon-on-the-woman-at-the-well-3-27-11/"
><svg class="photon-icon back" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"
><!--
- This <svg> element is derrived from “back-16.svg”
- from Mozilla’s “Photon Icons” collection.
- It is subject to the terms of the Mozilla Public License, v. 2.0.
- If a copy of the MPL was not distributed with this file,
- you can obtain one at <https://mozilla.org/MPL/2.0/>.
- * Source: <https://github.com/BrentHouse/photon-icons>
- * Upstream: <https://github.com/FirefoxUX/photon-icons>
--><path fill="context-fill" d="M15 7H3.414l4.293-4.293a1 1 0 0 0-1.414-1.414l-6 6a1 1 0 0 0 0 1.414l6 6a1 1 0 0 0 1.414-1.414L3.414 9H15a1 1 0 0 0 0-2z"
></path></svg><br
/>July 6, 2011<br
/><span class="nav-button-post-title"
>Sermon on the Woman at the Well - 3-27-11</span></a></p></div></aside></main><footer
><address
><span class="address-part"
><b
>Brent House</b><br
/>The Episcopal Center at the University of Chicago</span><span class="address-part"
>5540 South Woodlawn Avenue<br
/>Chicago, IL 60637</span><span class="address-part"
><a href="tel:+1.773.947.8744"
>(773) 947-8744</a></span></address><div class="footer-2block"
><div class="social-media-container"
><p
><a href="https://facebook.com/brent.house.chicago" target="_blank" class="facebook" rel="external noopener noreferrer"
><svg xmlns="http://www.w3.org/2000/svg" class="social-media-icon" viewBox="0 -1024 1024 1024"
><!--
- This <svg> element depicts the Facebook logo.
- It is used under their trademark policy.
--><g transform="matrix(1 0 0 -1 0 0)"
><path d="M1024 512C1024 794.77 794.77 1024 512 1024S0 794.77 0 512C0 256.445 187.23 44.629 432 6.22V364H302V512H432V624.801C432 753.121 508.438 824 625.391 824C681.406 824 740 814 740 814V688H675.438C611.836 688 592 648.531 592 608.043V512H734L711.301 364H592V6.22C836.77 44.629 1024 256.445 1024 512Z"
></path></g></svg><span
>Facebook</span></a></p><p
><a href="https://discord.gg/6c9CZKJ" rel="external"
><svg xmlns="http://www.w3.org/2000/svg" class="social-media-icon" viewBox="0 0 245 245"
><!--
- This <svg> element depicts the Discord logo.
- It is used under their trademark policy.
--><g
><path d="M104.4 103.9c-5.7 0-10.2 5-10.2 11.1s4.6 11.1 10.2 11.1c5.7 0 10.2-5 10.2-11.1.1-6.1-4.5-11.1-10.2-11.1zM140.9 103.9c-5.7 0-10.2 5-10.2 11.1s4.6 11.1 10.2 11.1c5.7 0 10.2-5 10.2-11.1s-4.5-11.1-10.2-11.1z"
></path><path d="M189.5 20h-134C44.2 20 35 29.2 35 40.6v135.2c0 11.4 9.2 20.6 20.5 20.6h113.4l-5.3-18.5 12.8 11.9 12.1 11.2 21.5 19V40.6c0-11.4-9.2-20.6-20.5-20.6zm-38.6 130.6s-3.6-4.3-6.6-8.1c13.1-3.7 18.1-11.9 18.1-11.9-4.1 2.7-8 4.6-11.5 5.9-5 2.1-9.8 3.5-14.5 4.3-9.6 1.8-18.4 1.3-25.9-.1-5.7-1.1-10.6-2.7-14.7-4.3-2.3-.9-4.8-2-7.3-3.4-.3-.2-.6-.3-.9-.5-.2-.1-.3-.2-.4-.3-1.8-1-2.8-1.7-2.8-1.7s4.8 8 17.5 11.8c-3 3.8-6.7 8.3-6.7 8.3-22.1-.7-30.5-15.2-30.5-15.2 0-32.2 14.4-58.3 14.4-58.3 14.4-10.8 28.1-10.5 28.1-10.5l1 1.2c-18 5.2-26.3 13.1-26.3 13.1s2.2-1.2 5.9-2.9c10.7-4.7 19.2-6 22.7-6.3.6-.1 1.1-.2 1.7-.2 6.1-.8 13-1 20.2-.2 9.5 1.1 19.7 3.9 30.1 9.6 0 0-7.9-7.5-24.9-12.7l1.4-1.6s13.7-.3 28.1 10.5c0 0 14.4 26.1 14.4 58.3 0 0-8.5 14.5-30.6 15.2z"
></path></g></svg><span
>Discord</span></a></p><p
><a href="https://instagram.com/brent_house" target="_blank" class="facebook" rel="external noopener noreferrer"
><svg xmlns="http://www.w3.org/2000/svg" class="social-media-icon" viewBox="0 -504 504 504"
><!--
- This <svg> element depicts the Instagram logo.
- It is used under their trademark policy.
--><g transform="matrix(1 0 0 -1 0 0)"
><path d="M251.922 503.83984C183.504 503.83984 174.926 503.550778 148.055 502.32422C121.238 501.10156 102.926 496.84375 86.9023 490.6172C70.3359 484.1758 56.2891 475.5625 42.2813 461.5586C28.2773 447.5547 19.6641 433.5039 13.2266 416.9375C6.99609 400.914 2.73828 382.602 1.51563 355.785C.289063 328.918 0 320.34 0 251.922S.289063 174.926 1.51563 148.055C2.73828 121.238 6.99609 102.926 13.2266 86.902C19.6641 70.336 28.2773 56.289 42.2813 42.281C56.2891 28.277 70.3359 19.664 86.9023 13.227C102.926 6.996 121.238 2.738 148.055 1.516C174.926 .289 183.504 0 251.922 0S328.918 .289 355.785 1.516C382.602 2.738 400.914 6.996 416.938 13.227C433.504 19.664 447.555 28.277 461.559 42.281C475.563 56.289 484.18 70.336 490.617 86.902C496.844 102.926 501.102 121.238 502.324 148.055C503.551 174.926 503.84 183.504 503.84 251.922S503.551 328.918 502.324 355.785C501.102 382.602 496.844 400.914 490.617 416.9375C484.18 433.5039 475.563 447.5547 461.559 461.5586S433.504 484.1758 416.938 490.6172C400.914 496.84375 382.602 501.10156 355.785 502.32422C328.918 503.550778 320.34 503.83984 251.922 503.83984ZM251.922 458.4492C319.188 458.4492 327.152 458.1953 353.719 456.9805C378.281 455.8594 391.617 451.7578 400.496 448.3086C412.254 443.7383 420.648 438.2773 429.461 429.4609C438.277 420.6484 443.738 412.2539 448.309 400.496C451.758 391.617 455.859 378.281 456.98 353.719C458.191 327.152 458.449 319.187 458.449 251.922C458.449 184.656 458.191 176.687 456.98 150.121C455.859 125.562 451.758 112.223 448.309 103.344C443.738 91.586 438.277 83.195 429.461 74.379C420.648 65.562 412.254 60.105 400.496 55.535C391.617 52.086 378.281 47.98 353.719 46.859C327.156 45.648 319.191 45.391 251.922 45.391C184.648 45.391 176.684 45.648 150.121 46.859C125.563 47.98 112.223 52.086 103.344 55.535C91.5859 60.105 83.1953 65.562 74.3789 74.379C65.5625 83.195 60.1055 91.586 55.5352 103.344C52.0859 112.223 47.9805 125.562 46.8594 150.121C45.6484 176.687 45.3906 184.656 45.3906 251.922C45.3906 319.187 45.6484 327.152 46.8594 353.719C47.9805 378.281 52.0859 391.617 55.5352 400.496C60.1055 412.2539 65.5625 420.6484 74.3789 429.4609C83.1953 438.2773 91.5859 443.7383 103.344 448.3086C112.223 451.7578 125.563 455.8594 150.121 456.9805C176.688 458.1953 184.656 458.4492 251.922 458.4492"
></path><path d="M251.922 167.945C205.543 167.945 167.945 205.543 167.945 251.922C167.945 298.297 205.543 335.895 251.922 335.895C298.297 335.895 335.895 298.297 335.895 251.922C335.895 205.543 298.297 167.945 251.922 167.945ZM251.922 381.285C180.473 381.285 122.555 323.367 122.555 251.922C122.555 180.473 180.473 122.555 251.922 122.555C323.367 122.555 381.285 180.473 381.285 251.922C381.285 323.367 323.367 381.285 251.922 381.285Z"
></path><path d="M416.629 386.395C416.629 369.699 403.094 356.168 386.395 356.168C369.699 356.168 356.164 369.699 356.164 386.395C356.164 403.094 369.699 416.625 386.395 416.625C403.094 416.625 416.629 403.094 416.629 386.395Z"
></path></g></svg><span
>Instagram</span></a></p><p
><a href="https://twitter.com/BrentHouseEpisc" rel="external"
><svg xmlns="http://www.w3.org/2000/svg" class="social-media-icon" viewBox="0 0 400 400"
><!--
- This <svg> element depicts the Twitter logo.
- It is used under their trademark policy.
--><path d="M153.6,301.6c94.3,0,145.9-78.2,145.9-145.9c0-2.2,0-4.4-0.1-6.6c10-7.2,18.7-16.3,25.6-26.6
			c-9.2,4.1-19.1,6.8-29.5,8.1c10.6-6.3,18.7-16.4,22.6-28.4c-9.9,5.9-20.9,10.1-32.6,12.4c-9.4-10-22.7-16.2-37.4-16.2
			c-28.3,0-51.3,23-51.3,51.3c0,4,0.5,7.9,1.3,11.7c-42.6-2.1-80.4-22.6-105.7-53.6c-4.4,7.6-6.9,16.4-6.9,25.8
			c0,17.8,9.1,33.5,22.8,42.7c-8.4-0.3-16.3-2.6-23.2-6.4c0,0.2,0,0.4,0,0.7c0,24.8,17.7,45.6,41.1,50.3c-4.3,1.2-8.8,1.8-13.5,1.8
			c-3.3,0-6.5-0.3-9.6-0.9c6.5,20.4,25.5,35.2,47.9,35.6c-17.6,13.8-39.7,22-63.7,22c-4.1,0-8.2-0.2-12.2-0.7
			C97.7,293.1,124.7,301.6,153.6,301.6"
></path></svg><span
>Twitter</span></a></p></div><div class="donate-and-subscribe-buttons-container"
><p class="basic-button-container"
><a href="https://brenthouse.z2systems.com/np/clients/brenthouse/donation.jsp"
>Donate to Brent House</a></p><p class="basic-button-container"
><a href="https://newsletter.brenthouse.org"
>Subscribe to our Newsletter</a></p><p class="basic-button-container"
><a href="https://brenthouse.z2systems.com/np/clients/brenthouse/login.jsp"
>Update Contact Information</a></p></div></div><nav class="site-nav"
><ul
><li
><a href="/"
>Welcome</a></li><li
><a href="/services.html"
>Services &amp; Programs</a></li><li
><a href="/blog/"
>Blog</a></li><li
><a href="/leadership.html"
>Clergy &amp; Leadership</a></li><li
><a href="/residents-and-guests.html"
>Living or Staying at Brent House</a></li></ul></nav></footer></body></html>
