<!DOCTYPE html>
<html lang="en" class="has-lhs"
><head
><title
>Sermon by Laura Eberly, November 18, 2007 | Brent House</title><meta charset="utf-8"
/><meta name="viewport" content="width=device-width,initial-scale=1"
/><meta name="format-detection" content="telephone=no"
/><link rel="stylesheet" href="/style/fonts/fonts.css"
/><link rel="stylesheet" href="/style/old-brenthouse.css"
/><link rel="icon" type="image/svg+xml" href="/style/heresy-shield.svg"
/></head><body
><header class="site-header"
><h1
>Brent House</h1><p
>The Episcopal Center at the University of Chicago</p><div class="site-logo"
><a href="/"
><img src="/style/heresy-shield.svg" alt="Brent House’s logo"
/></a></div></header><nav class="site-nav"
><ul
><li
><a href="/"
>Welcome</a></li><li
><a href="/services.html"
>Services &amp; Programs</a></li><li
><a href="/blog/"
>Blog</a></li><li
><a href="/leadership.html"
>Clergy &amp; Leadership</a></li><li
><a href="/residents-and-guests.html"
>Living or Staying at Brent House</a></li></ul></nav><main class="page-container"
><header
><h2
>Sermon by Laura Eberly, November 18, 2007</h2></header><div class="page-body"
><h3
>Tuesday, December 4, 2007</h3><p
>I was a little worried writing this sermon, because the first line of the gospel to jump out at me was, “This will give you an opportunity to testify. So make up your minds not to prepare your defense in advance.”</p><p
>When I got past that I looked at the gospel and went, “Oh great, it’s the apocalypse.” I didn’t want to preach about the apocalypse. The whole fire and brimstone aspect of God’s judgment is something I haven’t completely reconciled with my understanding of grace – it’s always been an angry side of a loving God that I don’t like to deal with. But I figured I’d let it sit until 8th week, and, as reliably happens at U of C, 8th week got closer and so did the fiery end of the world. In the middle of relationships falling apart, apocalyptic midterms and an unrehearsed choir with a furious director, I found that point last week where I just wanted to be done. That point when gray and damp seep out of the buildings and onto the streets, when life is just hard. The point when all I want is to go and be with God. When that love pulls me, suggests abandoning my difficult, broken relationships, abandoning my work and that difficult search for meaning in it to go and be wrapped warmly and finally in that love. The disciples in Luke’s gospel understand that desire. When is the end coming? They ask him. When is that time of relief when everything will be made right again, ordered according to God’s plan? Something in their question is eager, echoes of Malachi who understands the end as a time of ultimate retribution when the wrongs of the world will be righted.</p><p
>So I, like the disciples, started looking to Jesus for answers. But he doesn’t answer the question. We think we’re getting an answer; after. After nations rise against nations, after betrayal, famine and plague. When will this happen? We know when the temple will be destroyed; 70 AD, when the Romans take Jerusalem or 8th week, about two hours before my econ midterm. And yet, not quite. Because these chaotic collapses are always happening. People and their societies are always fighting, always falling apart, always hurting one another. And the end does not come immediately after, Jesus tells us. Just after. If those things are happening now, and they always have been happening now, his answer doesn’t tell us anything. We pass through these points like the uptaken breath before midnight on New Year’s Eve 1999; then we deflate and unsquinch our eyes moments later to find the world has not in fact ended. We are in 9th week, or the year 2000. We are disappointed. Just a little bit. Because then we would have had answers. We would know. Jesus doesn’t give us the satisfaction.</p><p
>“Don’t worry about when that will be,” he tells us. “First, you have something more real to deal with. Deal with your lives, because you will be betrayed and put in prisons and called to testify in my name.” That’s not what I was asking. I was asking, when do I get to be done? I was calling you to account, saying, “When are you going to come back and fix this broken world?!” There are people who sleep on the heat vents on the Midway. There are wealthy neighborhoods on the Northside that are unrecognizable as part of the Chicago I live in every day. The cellphone in my pocket is the complicated product of sweat shop labor in Taiwan and a civil war in the Congo. I ate today. My second grade student, Thabang, and his family didn’t. God alone can fix this world.</p><p
>In this gospel, wars, plagues, famines and betrayals do not signal the end of the world. Those dark things are part of this world we live in. Before the end of the world, they will happen. But they do not signal an imminent coming. Don’t spend your time figuring out when the apocalypse will come, counting days, pages or problem sets until break or graduation. He tells his disciples, you cannot rely on food, health, states, communities, families or your own expectations. These structures will not always be here. This is true for the future, and true in the past and true now. Human creations fail, have failed, will fail. Jesus tells us, I do not fail. I am always and constantly with you, if you make up your minds to accept me. If you stop preparing for something else and live presently in the world around you. Endure, he says. We have from Thessalonians this command to work. “Do not be weary in doing what is right.” Even when I have taught Itumeleng this sum seventeen times, in seven different ways, Jesus calls on me to have as much patience and love the eighteenth time I teach him as I should have had the first. Even when there is this longing to be done, here is Jesus saying, “No. Stay and work, and testify. And it will not be easy, or pleasant or without pain. But it is what you have to do.” We cannot wait for Christ to come. Because we are in a broken world. But we are in it. And Christ condemning the state of our world does not condemn our contributions to it.</p><p
>The verses of this chapter before the reading recount the widow, who gives a coin from her meager savings to the temple. Jesus praises her gift as greater than the gifts of those who give out of their wealth, but some read the story as a condemnation of the temple system, which forced the marginalized to offer money they did not have. The morality of the system, the temple’s structure, is profoundly questionable. But the widow’s contribution is not ruined by that fact. She cannot change the world, nor can we. We cannot end hunger. But we also cannot know God’s plan for bringing about its eventual demise. We are not supposed to. If we focus on ends – on creating significant political or economic change – we burn out easily, we quail at the futility of our efforts and lives. But we can continue to do the work, only because to do nothing is impossible, and because we know we find God in it. We are called by Christ to work with him and in him.</p><p
>This is not an exhortation to put our heads down and work without asking why. Rather, it is offering a way to challenge that unanswerable why with an answer that does not abandon us to whether it’s a fulfilling optimistic day or not. We work for and with the God who made us. We endure each day to carry out whatever plan he has for us in a plan infinitely greater than what we can imagine or predict. That paradise of dimensions we cannot comprehend that we talked about last week. And we cannot be wearied by the fact that we do not immediately see that kingdom coming with our efforts. Christ calls us to have faith that, even in prison or finals week, he is with us, giving us words and unshakeable justification.</p><p
>In the final line of today’s reading, Jesus says, “By your endurance you will gain your souls.” The word “souls” is in some places translated as lives. The Greek incorporates a sense of wholeness, personhood and integrity. By only anticipating the end of this life apart from God, Jesus tells us, we are in some sense not whole people. By endurance through this inescapable world, we come to call on Christ in the present. We require him as the only certain source of meaning and reliability. We know this in our own lives. It is by our trials that we learn to appreciate God and Christ’s sacrifice more dearly. God has saved my life more than once, and I have seldom felt closer to him than in these moments. We discover God in new ways when we fight with him, when we demand his presence and strength. He calls us to draw closer to him in that faith, relying on him alone. My teachers in South Africa knew God intimately because there was nothing else they could rely on. When food was on the table, it was often only and obviously a work of God. By our work in faith, we incorporate Christ into our actions as our only meaning and into our very beings. By our endurance, I would venture, we bring Christ eventually into the world.</p></div><aside
><div class="blog-nav-buttons-container"
><p class="basic-button-container"
><a href="/2007/12/"
><span class="rotate-quarter-turn"
><svg class="photon-icon back" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"
><!--
- This <svg> element is derrived from “back-16.svg”
- from Mozilla’s “Photon Icons” collection.
- It is subject to the terms of the Mozilla Public License, v. 2.0.
- If a copy of the MPL was not distributed with this file,
- you can obtain one at <https://mozilla.org/MPL/2.0/>.
- * Source: <https://github.com/BrentHouse/photon-icons>
- * Upstream: <https://github.com/FirefoxUX/photon-icons>
--><path fill="context-fill" d="M15 7H3.414l4.293-4.293a1 1 0 0 0-1.414-1.414l-6 6a1 1 0 0 0 0 1.414l6 6a1 1 0 0 0 1.414-1.414L3.414 9H15a1 1 0 0 0 0-2z"
></path></svg></span><br
/>Browse Archive</a></p><p class="basic-button-container"
><a href="/2009/02/22/thanks-to-charlie-neil/"
><svg class="photon-icon forward" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"
><!--
- This <svg> element is derrived from “forward-16.svg”
- from Mozilla’s “Photon Icons” collection.
- It is subject to the terms of the Mozilla Public License, v. 2.0.
- If a copy of the MPL was not distributed with this file,
- you can obtain one at <https://mozilla.org/MPL/2.0/>.
- * Source: <https://github.com/BrentHouse/photon-icons>
- * Upstream: <https://github.com/FirefoxUX/photon-icons>
--><path fill="context-fill" d="M15.707 7.293l-6-6a1 1 0 0 0-1.414 1.414L12.586 7H1a1 1 0 0 0 0 2h11.586l-4.293 4.293a1 1 0 1 0 1.414 1.414l6-6a1 1 0 0 0 0-1.414z"
></path></svg><br
/>February 22, 2009<br
/><span class="nav-button-post-title"
>Thanks to Charlie &amp; Neil!</span></a></p><p class="basic-button-container"
><a href="/2007/11/26/hail-thee-festival-day-convention-version/"
><svg class="photon-icon back" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"
><!--
- This <svg> element is derrived from “back-16.svg”
- from Mozilla’s “Photon Icons” collection.
- It is subject to the terms of the Mozilla Public License, v. 2.0.
- If a copy of the MPL was not distributed with this file,
- you can obtain one at <https://mozilla.org/MPL/2.0/>.
- * Source: <https://github.com/BrentHouse/photon-icons>
- * Upstream: <https://github.com/FirefoxUX/photon-icons>
--><path fill="context-fill" d="M15 7H3.414l4.293-4.293a1 1 0 0 0-1.414-1.414l-6 6a1 1 0 0 0 0 1.414l6 6a1 1 0 0 0 1.414-1.414L3.414 9H15a1 1 0 0 0 0-2z"
></path></svg><br
/>November 26, 2007<br
/><span class="nav-button-post-title"
>Hail Thee, Festival Day (Convention Version)</span></a></p></div></aside></main><footer
><address
><span class="address-part"
><b
>Brent House</b><br
/>The Episcopal Center at the University of Chicago</span><span class="address-part"
>5540 South Woodlawn Avenue<br
/>Chicago, IL 60637</span><span class="address-part"
><a href="tel:+1.773.947.8744"
>(773) 947-8744</a></span></address><div class="footer-2block"
><div class="social-media-container"
><p
><a href="https://facebook.com/brent.house.chicago" target="_blank" class="facebook" rel="external noopener noreferrer"
><svg xmlns="http://www.w3.org/2000/svg" class="social-media-icon" viewBox="0 -1024 1024 1024"
><!--
- This <svg> element depicts the Facebook logo.
- It is used under their trademark policy.
--><g transform="matrix(1 0 0 -1 0 0)"
><path d="M1024 512C1024 794.77 794.77 1024 512 1024S0 794.77 0 512C0 256.445 187.23 44.629 432 6.22V364H302V512H432V624.801C432 753.121 508.438 824 625.391 824C681.406 824 740 814 740 814V688H675.438C611.836 688 592 648.531 592 608.043V512H734L711.301 364H592V6.22C836.77 44.629 1024 256.445 1024 512Z"
></path></g></svg><span
>Facebook</span></a></p><p
><a href="https://discord.gg/6c9CZKJ" rel="external"
><svg xmlns="http://www.w3.org/2000/svg" class="social-media-icon" viewBox="0 0 245 245"
><!--
- This <svg> element depicts the Discord logo.
- It is used under their trademark policy.
--><g
><path d="M104.4 103.9c-5.7 0-10.2 5-10.2 11.1s4.6 11.1 10.2 11.1c5.7 0 10.2-5 10.2-11.1.1-6.1-4.5-11.1-10.2-11.1zM140.9 103.9c-5.7 0-10.2 5-10.2 11.1s4.6 11.1 10.2 11.1c5.7 0 10.2-5 10.2-11.1s-4.5-11.1-10.2-11.1z"
></path><path d="M189.5 20h-134C44.2 20 35 29.2 35 40.6v135.2c0 11.4 9.2 20.6 20.5 20.6h113.4l-5.3-18.5 12.8 11.9 12.1 11.2 21.5 19V40.6c0-11.4-9.2-20.6-20.5-20.6zm-38.6 130.6s-3.6-4.3-6.6-8.1c13.1-3.7 18.1-11.9 18.1-11.9-4.1 2.7-8 4.6-11.5 5.9-5 2.1-9.8 3.5-14.5 4.3-9.6 1.8-18.4 1.3-25.9-.1-5.7-1.1-10.6-2.7-14.7-4.3-2.3-.9-4.8-2-7.3-3.4-.3-.2-.6-.3-.9-.5-.2-.1-.3-.2-.4-.3-1.8-1-2.8-1.7-2.8-1.7s4.8 8 17.5 11.8c-3 3.8-6.7 8.3-6.7 8.3-22.1-.7-30.5-15.2-30.5-15.2 0-32.2 14.4-58.3 14.4-58.3 14.4-10.8 28.1-10.5 28.1-10.5l1 1.2c-18 5.2-26.3 13.1-26.3 13.1s2.2-1.2 5.9-2.9c10.7-4.7 19.2-6 22.7-6.3.6-.1 1.1-.2 1.7-.2 6.1-.8 13-1 20.2-.2 9.5 1.1 19.7 3.9 30.1 9.6 0 0-7.9-7.5-24.9-12.7l1.4-1.6s13.7-.3 28.1 10.5c0 0 14.4 26.1 14.4 58.3 0 0-8.5 14.5-30.6 15.2z"
></path></g></svg><span
>Discord</span></a></p><p
><a href="https://instagram.com/brent_house" target="_blank" class="facebook" rel="external noopener noreferrer"
><svg xmlns="http://www.w3.org/2000/svg" class="social-media-icon" viewBox="0 -504 504 504"
><!--
- This <svg> element depicts the Instagram logo.
- It is used under their trademark policy.
--><g transform="matrix(1 0 0 -1 0 0)"
><path d="M251.922 503.83984C183.504 503.83984 174.926 503.550778 148.055 502.32422C121.238 501.10156 102.926 496.84375 86.9023 490.6172C70.3359 484.1758 56.2891 475.5625 42.2813 461.5586C28.2773 447.5547 19.6641 433.5039 13.2266 416.9375C6.99609 400.914 2.73828 382.602 1.51563 355.785C.289063 328.918 0 320.34 0 251.922S.289063 174.926 1.51563 148.055C2.73828 121.238 6.99609 102.926 13.2266 86.902C19.6641 70.336 28.2773 56.289 42.2813 42.281C56.2891 28.277 70.3359 19.664 86.9023 13.227C102.926 6.996 121.238 2.738 148.055 1.516C174.926 .289 183.504 0 251.922 0S328.918 .289 355.785 1.516C382.602 2.738 400.914 6.996 416.938 13.227C433.504 19.664 447.555 28.277 461.559 42.281C475.563 56.289 484.18 70.336 490.617 86.902C496.844 102.926 501.102 121.238 502.324 148.055C503.551 174.926 503.84 183.504 503.84 251.922S503.551 328.918 502.324 355.785C501.102 382.602 496.844 400.914 490.617 416.9375C484.18 433.5039 475.563 447.5547 461.559 461.5586S433.504 484.1758 416.938 490.6172C400.914 496.84375 382.602 501.10156 355.785 502.32422C328.918 503.550778 320.34 503.83984 251.922 503.83984ZM251.922 458.4492C319.188 458.4492 327.152 458.1953 353.719 456.9805C378.281 455.8594 391.617 451.7578 400.496 448.3086C412.254 443.7383 420.648 438.2773 429.461 429.4609C438.277 420.6484 443.738 412.2539 448.309 400.496C451.758 391.617 455.859 378.281 456.98 353.719C458.191 327.152 458.449 319.187 458.449 251.922C458.449 184.656 458.191 176.687 456.98 150.121C455.859 125.562 451.758 112.223 448.309 103.344C443.738 91.586 438.277 83.195 429.461 74.379C420.648 65.562 412.254 60.105 400.496 55.535C391.617 52.086 378.281 47.98 353.719 46.859C327.156 45.648 319.191 45.391 251.922 45.391C184.648 45.391 176.684 45.648 150.121 46.859C125.563 47.98 112.223 52.086 103.344 55.535C91.5859 60.105 83.1953 65.562 74.3789 74.379C65.5625 83.195 60.1055 91.586 55.5352 103.344C52.0859 112.223 47.9805 125.562 46.8594 150.121C45.6484 176.687 45.3906 184.656 45.3906 251.922C45.3906 319.187 45.6484 327.152 46.8594 353.719C47.9805 378.281 52.0859 391.617 55.5352 400.496C60.1055 412.2539 65.5625 420.6484 74.3789 429.4609C83.1953 438.2773 91.5859 443.7383 103.344 448.3086C112.223 451.7578 125.563 455.8594 150.121 456.9805C176.688 458.1953 184.656 458.4492 251.922 458.4492"
></path><path d="M251.922 167.945C205.543 167.945 167.945 205.543 167.945 251.922C167.945 298.297 205.543 335.895 251.922 335.895C298.297 335.895 335.895 298.297 335.895 251.922C335.895 205.543 298.297 167.945 251.922 167.945ZM251.922 381.285C180.473 381.285 122.555 323.367 122.555 251.922C122.555 180.473 180.473 122.555 251.922 122.555C323.367 122.555 381.285 180.473 381.285 251.922C381.285 323.367 323.367 381.285 251.922 381.285Z"
></path><path d="M416.629 386.395C416.629 369.699 403.094 356.168 386.395 356.168C369.699 356.168 356.164 369.699 356.164 386.395C356.164 403.094 369.699 416.625 386.395 416.625C403.094 416.625 416.629 403.094 416.629 386.395Z"
></path></g></svg><span
>Instagram</span></a></p><p
><a href="https://twitter.com/BrentHouseEpisc" rel="external"
><svg xmlns="http://www.w3.org/2000/svg" class="social-media-icon" viewBox="0 0 400 400"
><!--
- This <svg> element depicts the Twitter logo.
- It is used under their trademark policy.
--><path d="M153.6,301.6c94.3,0,145.9-78.2,145.9-145.9c0-2.2,0-4.4-0.1-6.6c10-7.2,18.7-16.3,25.6-26.6
			c-9.2,4.1-19.1,6.8-29.5,8.1c10.6-6.3,18.7-16.4,22.6-28.4c-9.9,5.9-20.9,10.1-32.6,12.4c-9.4-10-22.7-16.2-37.4-16.2
			c-28.3,0-51.3,23-51.3,51.3c0,4,0.5,7.9,1.3,11.7c-42.6-2.1-80.4-22.6-105.7-53.6c-4.4,7.6-6.9,16.4-6.9,25.8
			c0,17.8,9.1,33.5,22.8,42.7c-8.4-0.3-16.3-2.6-23.2-6.4c0,0.2,0,0.4,0,0.7c0,24.8,17.7,45.6,41.1,50.3c-4.3,1.2-8.8,1.8-13.5,1.8
			c-3.3,0-6.5-0.3-9.6-0.9c6.5,20.4,25.5,35.2,47.9,35.6c-17.6,13.8-39.7,22-63.7,22c-4.1,0-8.2-0.2-12.2-0.7
			C97.7,293.1,124.7,301.6,153.6,301.6"
></path></svg><span
>Twitter</span></a></p></div><div class="donate-and-subscribe-buttons-container"
><p class="basic-button-container"
><a href="https://brenthouse.z2systems.com/np/clients/brenthouse/donation.jsp"
>Donate to Brent House</a></p><p class="basic-button-container"
><a href="https://newsletter.brenthouse.org"
>Subscribe to our Newsletter</a></p><p class="basic-button-container"
><a href="https://brenthouse.z2systems.com/np/clients/brenthouse/login.jsp"
>Update Contact Information</a></p></div></div><nav class="site-nav"
><ul
><li
><a href="/"
>Welcome</a></li><li
><a href="/services.html"
>Services &amp; Programs</a></li><li
><a href="/blog/"
>Blog</a></li><li
><a href="/leadership.html"
>Clergy &amp; Leadership</a></li><li
><a href="/residents-and-guests.html"
>Living or Staying at Brent House</a></li></ul></nav></footer></body></html>
