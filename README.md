brenthouse.gitlab.io
===============================================================

GitLab Pages repository for Brent House,
the Episcopal Campus Ministry at the University of Chicago.
This is the source for the website at <https://brenthouse.org>.


Software License
----------------

This repository contains free/libre and open-source software
developed for Brent House.
Permission is granted to use, redistribute, and/or modify this software
under the terms of the Apache License, Version 2.0 (the "License");
you may not use this software except in compliance with the License.

You should have received a copy of the License along with this software.
If not, see <https://www.apache.org/licenses/LICENSE-2.0>.

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an **"as is" basis,
without warranties or conditions of any kind,** either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

By making a contribution to the software in this repository,
you are agreeing that your contribution is licensed under
the Apache 2.0 license.

Note that this permission applies exclusively to software,
**not** to prose, photographs, or other data in this repository.
Furthermore, the software may include components licensed under
other free and open-source licesnces compatable with the Apache 2.0 license.
