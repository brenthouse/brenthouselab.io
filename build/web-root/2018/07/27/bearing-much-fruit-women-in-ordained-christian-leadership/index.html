<!DOCTYPE html>
<html lang="en" class="has-lhs"
><head
><title
>Bearing Much Fruit: Women in Ordained Christian Leadership | Brent House</title><meta charset="utf-8"
/><meta name="viewport" content="width=device-width,initial-scale=1"
/><meta name="format-detection" content="telephone=no"
/><link rel="stylesheet" href="/style/fonts/fonts.css"
/><link rel="stylesheet" href="/style/old-brenthouse.css"
/><link rel="icon" type="image/svg+xml" href="/style/heresy-shield.svg"
/></head><body
><header class="site-header"
><h1
>Brent House</h1><p
>The Episcopal Center at the University of Chicago</p><div class="site-logo"
><a href="/"
><img src="/style/heresy-shield.svg" alt="Brent House’s logo"
/></a></div></header><nav class="site-nav"
><ul
><li
><a href="/"
>Welcome</a></li><li
><a href="/services.html"
>Services &amp; Programs</a></li><li
><a href="/blog/"
>Blog</a></li><li
><a href="/leadership.html"
>Clergy &amp; Leadership</a></li><li
><a href="/residents-and-guests.html"
>Living or Staying at Brent House</a></li></ul></nav><main class="page-container"
><header
><h2
>Bearing Much Fruit: Women in Ordained Christian Leadership</h2></header><div class="page-body"
><h3
>Friday, July 27, 2018</h3><p
><i
>This is an essay Stacy wrote to be included in the appendix of the </i>Da Vinci Code-<i
>like thriller, </i>Deborah’s Number: A Bank Heist Mitzvah, <i
>by Ezra Barany, along with other essays about women in Islam and Judaism.</i></p><p
>I knew I wanted to be a pastor when I was ten years old. I was a shy, nerdy, earnest child, who loved singing in the choir, and who—respectfully—talked back to the pastor when he seemed to have missed an important point. That was 1974. We were not yet Episcopalians, the denomination in which I would eventually be ordained, and the Episcopal Church did not yet officially ordain women to the priesthood. But it never occurred to me that I could not serve God and the world in that way. I had never met a woman pastor, but neither had I been told that being female kept me from the ministry—or anything else, for that matter. For that I thank my parents. They had the traditional white middle-class marriage: Dad worked outside of the house and my mom stayed home. It worked for them. But never did either of them tell me, either implicitly or explicitly, that any work in the world was unavailable or unattainable for me. It was my Dad, in fact, who, despite being a very traditional guy, taught me that all rules are meant to be questioned and that it is the quality of one’s work, not what is or is not in one’s pants, that counts.</p><p
>My family moved a lot as I was growing up, and we attended a variety of mainline Protestant churches along the way. When we moved to Seattle and I began junior high school, we visited several congregations, including a period at the local Roman Catholic church, before my family found a home with the Episcopalians.</p><p
>Around that time, my parents sent me to a local evangelical summer camp, where we had Rapture drills (games that prepared us for being taken up into the sky at Jesus’ return) and learned how to evangelize our friends. This was also where I fell in love with the Bible, a romance that deepened in college when I joined a student-led evangelical group and learned to lead Bible studies, and which continues today. Conservative evangelical theology worked for me, giving me clear answers—until I began to have the feeling that there were questions that weren’t supposed to be asked, or for which I was given unsatisfying answers, questions like, “Why is there evil in the world?”, “Is Jesus the only way to God?” and “What about my gay and lesbian friends?”</p><p
>It was here that the Jesuits at my university “saved” me. I encountered men who were passionately in love with a God who was big enough for any question, any objection, any doubt they could throw. I was encouraged to take on spiritual leadership and learned how the rituals of the faith could include my body and heart, as well as my mind and soul. I fell in with a group of Roman Catholic women who wrestled with the Church’s teaching about women’s ordination and experimented with rituals and prayers that affirmed them, just as Jesus had affirmed the women who followed him. I attended a debate on women’s ordination. Of all of the Jesuit priests at my college, only one was found to argue against it. When I told a chaplain of my desire to be ordained, he proclaimed that he would be in the front row, cheering me on.</p><p
>But it was also here that I saw that the institution of the Church would fight change. While I was in college, Pope John Paul II declared women’s ordination to be a closed topic. People I loved and respected, particularly the women, struggled to be faithful to the Church they loved, even as they felt betrayed that their sense of call had been definitively negated. I saw a beloved mentor be maligned and discredited for revealing that she had been assaulted by a priest.</p><p
>Through all of this my own sense of call did not waver. It seemed inevitable, although the path ahead was not always clear.</p><p
>In my first parish, it took a little while to figure out how to embody my priesthood. I already had had my first child, and became pregnant with my second while I was there. I preached Christmas Eve with my swollen belly (and ankles!) and performed a baptism (which is a ritual full of birth imagery) the Sunday before my daughter was born.</p><p
>In my tradition, clergy are referred to as “icons,” windows through which the faithful can see the possibilities for faithfulness and redemption. It has been so important for members of my congregations to see a woman do priestly things, and for their priest to speak of womanly things. When St. Paul talks about caring for the community like a nursing mother (1 Thessalonians 2:7), I preach as one who has nursed her children. My experience as a mother enabled me to preach not just the nurturing, gentle side of God’s parenthood, but also the protective, frustrated, broken-hearted side. One day a parishioner told me that her daughter had put a towel around her neck, superhero style, and paraded around the house, declaring, “I’m Mother Stacy!” She wasn’t pretending to wear a superhero cape, though, but rather a cope, the ceremonial cape I would wear on special occasions. This little girl, no matter where her path leads her, will always have that image of a woman, garbed in robes of both authority and beauty, preaching the Gospel and leading her community in worship.</p><p
>As a female priest, it has been important also for me to talk openly, from the pulpit as well as in one-on-one conversations, about the fact that my first marriage was abusive and dysfunctional. Domestic violence affects and traps women in particular ways, and without women with authority and a literal pulpit, those experiences will always be pushed to the margins, flattened, caricatured. Speaking from my own experience, I can be clear both that I was not to blame for the abuse and also that I am accountable for ways in which I allowed myself to be passive and ruled by fear. I can name some of those things as sinful, while affirming that the decisions I made for my self-preservation and that of my children—including divorcing my husband and remarrying—were virtuous and right.</p><p
>Many Christian communities do not have women in leadership. Many actively limit women’s voices and circumscribe their behavior. But the witness of Jesus himself and of the early Church show us another vision, one of strong, vocal, empowered women, and the ministry and witness of female leaders today show that the Gospel calls <i
>all</i> of us to freedom, <i
>all</i> of us to tell our stories. Without those stories the Body of Christ, the Church, will not be complete or whole. As Paul writes, “The eye cannot say to the hand, ‘I have no need of you’ nor again the head to the feet, ‘I have no need of you.’” The work of proclaiming the good news of God’s love for all, embodied in Jesus, requires the full participation of all. No matter what they have in their pants.</p><hr
/><p
>Women in the New Testament and Early Church</p><p
>Some argue that women ought not teach in the Church—or at least not teach men. They forget the time when a foreign woman (Syrophoenician in Mark, and Canaanite in Matthew) asks Jesus to cast a demon out of her daughter. He responds, “Let the children be fed first, for it is not fair to take the children’s food and throw it to the dogs,” a shocking statement for those of us who treasure Jesus’ kindness and generosity. The woman replies, “Sir, even the dogs under the table eat the children’s crumbs,” a gentle but firm challenge to his limited vision. Jesus sends her away with the word, “For saying that, you may go—the demon has left your daughter.” Some argue that he was testing her, but a more compelling interpretation is that he allowed this woman to teach him, to open his mind and heart to his own prejudices. She had faith that he would do the right thing. They also forget the Samaritan woman who engages in a theological argument with Jesus next to a well and literally brings her whole village to listen to him.</p><p
>There was something in what Jesus did and taught, and something in the early Christian communities, that drew women from all levels of society. Women responded to him, even challenged him, and women were essential to the growth and stability of the church in its early years.</p><p
>Women followed Jesus to the very end, staying with him even as he hung dying on the cross, after the men had fled. They were the first at the tomb. Some (too many) are nameless, like Peter’s mother-in-law and the Samaritan woman at the well, and some are named but underappreciated, like Mary Magdalene, and the sisters Mary and Martha. (Note that Mary and Martha have long conversations, even arguments, with Jesus, while their brother Lazarus never speaks a word.)</p><p
>Paul’s statements about women in his letters can be difficult, but his references to actual women in the Church show that he clearly saw women as co-workers in proclaiming the Gospel and caring for the community. Well-off women like Phoebe (Romans 16:1-2) and Chloé (1 Cor. 1:11) offered their homes for the local Christian communities to meet. Prisca and Aquila, a married couple, are mentioned several times in the New Testament as missionaries and fellow workers with Paul. Prisca’s name is always listed first. Paul calls Phoebe a deacon (not a deacon<i
>ess</i>) (Romans 16:1) and Junia an apostle (Romans 16:7). The fact that Paul mentions the conflict between Euodia and Syntyche (Philippians 4:2-3) and asks them to reconcile tells of the influence of these two women in their community.</p><p
>The Church eventually succumbed to the pressures of the prevailing culture, particularly after it became the religion of the Roman Empire, but Scripture still holds clues to the influence and authority of women in the founding of the early Christian communities and the spread of the Gospel.</p></div><aside
><div class="blog-nav-buttons-container"
><p class="basic-button-container"
><a href="/2018/"
><span class="rotate-quarter-turn"
><svg class="photon-icon back" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"
><!--
- This <svg> element is derrived from “back-16.svg”
- from Mozilla’s “Photon Icons” collection.
- It is subject to the terms of the Mozilla Public License, v. 2.0.
- If a copy of the MPL was not distributed with this file,
- you can obtain one at <https://mozilla.org/MPL/2.0/>.
- * Source: <https://github.com/BrentHouse/photon-icons>
- * Upstream: <https://github.com/FirefoxUX/photon-icons>
--><path fill="context-fill" d="M15 7H3.414l4.293-4.293a1 1 0 0 0-1.414-1.414l-6 6a1 1 0 0 0 0 1.414l6 6a1 1 0 0 0 1.414-1.414L3.414 9H15a1 1 0 0 0 0-2z"
></path></svg></span><br
/>Browse Archive</a></p><p class="basic-button-container disabled"
><a
><svg class="photon-icon forward-disabled" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" width="16" height="16" viewBox="0 0 16 16" version="1.1" sodipodi:docname="photon-bh-forward-disabled-16.svg" inkscape:version="1.0.2 (e86c870879, 2021-01-15)"
><!--
- This <svg> element is derrived from “forward-16.svg”
- from Mozilla’s “Photon Icons” collection.
- It is subject to the terms of the Mozilla Public License, v. 2.0.
- If a copy of the MPL was not distributed with this file,
- you can obtain one at <https://mozilla.org/MPL/2.0/>.
- * Source: <https://github.com/BrentHouse/photon-icons>
- * Upstream: <https://github.com/FirefoxUX/photon-icons>
--><metadata
><rdf:RDF
><cc:Work rdf:about=""
><dc:format
>image/svg+xml</dc:format><dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage"
></dc:type><dc:title
>photon-bh-forward-disabled-16</dc:title><cc:license rdf:resource="https://mozilla.org/MPL/2.0/"
></cc:license></cc:Work></rdf:RDF></metadata><defs
></defs><sodipodi:namedview pagecolor="#ffffff" bordercolor="#666666" borderopacity="1" objecttolerance="10" gridtolerance="10" guidetolerance="10" inkscape:pageopacity="0" inkscape:pageshadow="2" inkscape:window-width="1177" inkscape:window-height="842" showgrid="true" inkscape:pagecheckerboard="true" inkscape:zoom="44.6875" inkscape:cx="9.8814685" inkscape:cy="8.5115385" inkscape:window-x="0" inkscape:window-y="0" inkscape:window-maximized="0" inkscape:document-rotation="0"
><inkscape:grid type="xygrid" empspacing="4"
></inkscape:grid></sodipodi:namedview><path d="M 14.029297,1 C 13.753982,0.992086 13.487592,1.098077 13.292969,1.2929688 L 11.5,3.0859375 9.7070307,1.2929688 C 8.7644221,0.37752906 7.3775286,1.7644226 8.2929683,2.7070312 L 10.085938,4.5 7.585937,7 H 0.9999995 c -1.333333,0 -1.333333,2 0,2 h 4.585938 l -4.292969,4.292969 c -0.390423,0.390507 -0.390423,1.023555 0,1.414062 0.390507,0.390424 1.023555,0.390424 1.414062,0 L 8.414062,9 c 10.253343,-10.2533431 -11.4862075,11.486205 6.292969,-6.2929688 0.390424,-0.390507 0.390424,-1.0235554 0,-1.4140624 C 14.526763,1.1124342 14.284309,1.007627 14.029297,1 Z" sodipodi:nodetypes="cccccccsscccccccc" fill="context-fill"
></path><path d="M 13.712891 5.2988281 L 12.298828 6.7128906 L 12.585938 7 L 12.011719 7 L 10.011719 9 L 12.585938 9 L 8.2929688 13.292969 C 7.3223512 14.235574 8.7644263 15.677649 9.7070312 14.707031 L 15.707031 8.7070312 C 16.097455 8.3165243 16.097455 7.6834758 15.707031 7.2929688 L 13.712891 5.2988281 z " fill="context-fill"
></path><path d="M 14.597203,2.9999995 2.5972028,15" fill="context-fill"
></path></svg><br
/>No Newer Posts</a></p><p class="basic-button-container"
><a href="/2016/08/25/presiding-bishop-michael-curry-statement-in-support-of-the-advocacy-of-the-people-of-standing-rock-sioux-reservation/"
><svg class="photon-icon back" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"
><!--
- This <svg> element is derrived from “back-16.svg”
- from Mozilla’s “Photon Icons” collection.
- It is subject to the terms of the Mozilla Public License, v. 2.0.
- If a copy of the MPL was not distributed with this file,
- you can obtain one at <https://mozilla.org/MPL/2.0/>.
- * Source: <https://github.com/BrentHouse/photon-icons>
- * Upstream: <https://github.com/FirefoxUX/photon-icons>
--><path fill="context-fill" d="M15 7H3.414l4.293-4.293a1 1 0 0 0-1.414-1.414l-6 6a1 1 0 0 0 0 1.414l6 6a1 1 0 0 0 1.414-1.414L3.414 9H15a1 1 0 0 0 0-2z"
></path></svg><br
/>August 25, 2016<br
/><span class="nav-button-post-title"
>Presiding Bishop Michael Curry: Statement in support of the advocacy of the people of Standing Rock Sioux Reservation</span></a></p></div></aside></main><footer
><address
><span class="address-part"
><b
>Brent House</b><br
/>The Episcopal Center at the University of Chicago</span><span class="address-part"
>5540 South Woodlawn Avenue<br
/>Chicago, IL 60637</span><span class="address-part"
><a href="tel:+1.773.947.8744"
>(773) 947-8744</a></span></address><div class="footer-2block"
><div class="social-media-container"
><p
><a href="https://facebook.com/brent.house.chicago" target="_blank" class="facebook" rel="external noopener noreferrer"
><svg xmlns="http://www.w3.org/2000/svg" class="social-media-icon" viewBox="0 -1024 1024 1024"
><!--
- This <svg> element depicts the Facebook logo.
- It is used under their trademark policy.
--><g transform="matrix(1 0 0 -1 0 0)"
><path d="M1024 512C1024 794.77 794.77 1024 512 1024S0 794.77 0 512C0 256.445 187.23 44.629 432 6.22V364H302V512H432V624.801C432 753.121 508.438 824 625.391 824C681.406 824 740 814 740 814V688H675.438C611.836 688 592 648.531 592 608.043V512H734L711.301 364H592V6.22C836.77 44.629 1024 256.445 1024 512Z"
></path></g></svg><span
>Facebook</span></a></p><p
><a href="https://discord.gg/6c9CZKJ" rel="external"
><svg xmlns="http://www.w3.org/2000/svg" class="social-media-icon" viewBox="0 0 245 245"
><!--
- This <svg> element depicts the Discord logo.
- It is used under their trademark policy.
--><g
><path d="M104.4 103.9c-5.7 0-10.2 5-10.2 11.1s4.6 11.1 10.2 11.1c5.7 0 10.2-5 10.2-11.1.1-6.1-4.5-11.1-10.2-11.1zM140.9 103.9c-5.7 0-10.2 5-10.2 11.1s4.6 11.1 10.2 11.1c5.7 0 10.2-5 10.2-11.1s-4.5-11.1-10.2-11.1z"
></path><path d="M189.5 20h-134C44.2 20 35 29.2 35 40.6v135.2c0 11.4 9.2 20.6 20.5 20.6h113.4l-5.3-18.5 12.8 11.9 12.1 11.2 21.5 19V40.6c0-11.4-9.2-20.6-20.5-20.6zm-38.6 130.6s-3.6-4.3-6.6-8.1c13.1-3.7 18.1-11.9 18.1-11.9-4.1 2.7-8 4.6-11.5 5.9-5 2.1-9.8 3.5-14.5 4.3-9.6 1.8-18.4 1.3-25.9-.1-5.7-1.1-10.6-2.7-14.7-4.3-2.3-.9-4.8-2-7.3-3.4-.3-.2-.6-.3-.9-.5-.2-.1-.3-.2-.4-.3-1.8-1-2.8-1.7-2.8-1.7s4.8 8 17.5 11.8c-3 3.8-6.7 8.3-6.7 8.3-22.1-.7-30.5-15.2-30.5-15.2 0-32.2 14.4-58.3 14.4-58.3 14.4-10.8 28.1-10.5 28.1-10.5l1 1.2c-18 5.2-26.3 13.1-26.3 13.1s2.2-1.2 5.9-2.9c10.7-4.7 19.2-6 22.7-6.3.6-.1 1.1-.2 1.7-.2 6.1-.8 13-1 20.2-.2 9.5 1.1 19.7 3.9 30.1 9.6 0 0-7.9-7.5-24.9-12.7l1.4-1.6s13.7-.3 28.1 10.5c0 0 14.4 26.1 14.4 58.3 0 0-8.5 14.5-30.6 15.2z"
></path></g></svg><span
>Discord</span></a></p><p
><a href="https://instagram.com/brent_house" target="_blank" class="facebook" rel="external noopener noreferrer"
><svg xmlns="http://www.w3.org/2000/svg" class="social-media-icon" viewBox="0 -504 504 504"
><!--
- This <svg> element depicts the Instagram logo.
- It is used under their trademark policy.
--><g transform="matrix(1 0 0 -1 0 0)"
><path d="M251.922 503.83984C183.504 503.83984 174.926 503.550778 148.055 502.32422C121.238 501.10156 102.926 496.84375 86.9023 490.6172C70.3359 484.1758 56.2891 475.5625 42.2813 461.5586C28.2773 447.5547 19.6641 433.5039 13.2266 416.9375C6.99609 400.914 2.73828 382.602 1.51563 355.785C.289063 328.918 0 320.34 0 251.922S.289063 174.926 1.51563 148.055C2.73828 121.238 6.99609 102.926 13.2266 86.902C19.6641 70.336 28.2773 56.289 42.2813 42.281C56.2891 28.277 70.3359 19.664 86.9023 13.227C102.926 6.996 121.238 2.738 148.055 1.516C174.926 .289 183.504 0 251.922 0S328.918 .289 355.785 1.516C382.602 2.738 400.914 6.996 416.938 13.227C433.504 19.664 447.555 28.277 461.559 42.281C475.563 56.289 484.18 70.336 490.617 86.902C496.844 102.926 501.102 121.238 502.324 148.055C503.551 174.926 503.84 183.504 503.84 251.922S503.551 328.918 502.324 355.785C501.102 382.602 496.844 400.914 490.617 416.9375C484.18 433.5039 475.563 447.5547 461.559 461.5586S433.504 484.1758 416.938 490.6172C400.914 496.84375 382.602 501.10156 355.785 502.32422C328.918 503.550778 320.34 503.83984 251.922 503.83984ZM251.922 458.4492C319.188 458.4492 327.152 458.1953 353.719 456.9805C378.281 455.8594 391.617 451.7578 400.496 448.3086C412.254 443.7383 420.648 438.2773 429.461 429.4609C438.277 420.6484 443.738 412.2539 448.309 400.496C451.758 391.617 455.859 378.281 456.98 353.719C458.191 327.152 458.449 319.187 458.449 251.922C458.449 184.656 458.191 176.687 456.98 150.121C455.859 125.562 451.758 112.223 448.309 103.344C443.738 91.586 438.277 83.195 429.461 74.379C420.648 65.562 412.254 60.105 400.496 55.535C391.617 52.086 378.281 47.98 353.719 46.859C327.156 45.648 319.191 45.391 251.922 45.391C184.648 45.391 176.684 45.648 150.121 46.859C125.563 47.98 112.223 52.086 103.344 55.535C91.5859 60.105 83.1953 65.562 74.3789 74.379C65.5625 83.195 60.1055 91.586 55.5352 103.344C52.0859 112.223 47.9805 125.562 46.8594 150.121C45.6484 176.687 45.3906 184.656 45.3906 251.922C45.3906 319.187 45.6484 327.152 46.8594 353.719C47.9805 378.281 52.0859 391.617 55.5352 400.496C60.1055 412.2539 65.5625 420.6484 74.3789 429.4609C83.1953 438.2773 91.5859 443.7383 103.344 448.3086C112.223 451.7578 125.563 455.8594 150.121 456.9805C176.688 458.1953 184.656 458.4492 251.922 458.4492"
></path><path d="M251.922 167.945C205.543 167.945 167.945 205.543 167.945 251.922C167.945 298.297 205.543 335.895 251.922 335.895C298.297 335.895 335.895 298.297 335.895 251.922C335.895 205.543 298.297 167.945 251.922 167.945ZM251.922 381.285C180.473 381.285 122.555 323.367 122.555 251.922C122.555 180.473 180.473 122.555 251.922 122.555C323.367 122.555 381.285 180.473 381.285 251.922C381.285 323.367 323.367 381.285 251.922 381.285Z"
></path><path d="M416.629 386.395C416.629 369.699 403.094 356.168 386.395 356.168C369.699 356.168 356.164 369.699 356.164 386.395C356.164 403.094 369.699 416.625 386.395 416.625C403.094 416.625 416.629 403.094 416.629 386.395Z"
></path></g></svg><span
>Instagram</span></a></p><p
><a href="https://twitter.com/BrentHouseEpisc" rel="external"
><svg xmlns="http://www.w3.org/2000/svg" class="social-media-icon" viewBox="0 0 400 400"
><!--
- This <svg> element depicts the Twitter logo.
- It is used under their trademark policy.
--><path d="M153.6,301.6c94.3,0,145.9-78.2,145.9-145.9c0-2.2,0-4.4-0.1-6.6c10-7.2,18.7-16.3,25.6-26.6
			c-9.2,4.1-19.1,6.8-29.5,8.1c10.6-6.3,18.7-16.4,22.6-28.4c-9.9,5.9-20.9,10.1-32.6,12.4c-9.4-10-22.7-16.2-37.4-16.2
			c-28.3,0-51.3,23-51.3,51.3c0,4,0.5,7.9,1.3,11.7c-42.6-2.1-80.4-22.6-105.7-53.6c-4.4,7.6-6.9,16.4-6.9,25.8
			c0,17.8,9.1,33.5,22.8,42.7c-8.4-0.3-16.3-2.6-23.2-6.4c0,0.2,0,0.4,0,0.7c0,24.8,17.7,45.6,41.1,50.3c-4.3,1.2-8.8,1.8-13.5,1.8
			c-3.3,0-6.5-0.3-9.6-0.9c6.5,20.4,25.5,35.2,47.9,35.6c-17.6,13.8-39.7,22-63.7,22c-4.1,0-8.2-0.2-12.2-0.7
			C97.7,293.1,124.7,301.6,153.6,301.6"
></path></svg><span
>Twitter</span></a></p></div><div class="donate-and-subscribe-buttons-container"
><p class="basic-button-container"
><a href="https://brenthouse.z2systems.com/np/clients/brenthouse/donation.jsp"
>Donate to Brent House</a></p><p class="basic-button-container"
><a href="https://newsletter.brenthouse.org"
>Subscribe to our Newsletter</a></p><p class="basic-button-container"
><a href="https://brenthouse.z2systems.com/np/clients/brenthouse/login.jsp"
>Update Contact Information</a></p></div></div><nav class="site-nav"
><ul
><li
><a href="/"
>Welcome</a></li><li
><a href="/services.html"
>Services &amp; Programs</a></li><li
><a href="/blog/"
>Blog</a></li><li
><a href="/leadership.html"
>Clergy &amp; Leadership</a></li><li
><a href="/residents-and-guests.html"
>Living or Staying at Brent House</a></li></ul></nav></footer></body></html>
